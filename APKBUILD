# Contributor: Stuart Cardall <developer@it-offshore.co.uk>
# Maintainer: Stuart Cardall <developer@it-offshore.co.uk>
pkgname=modemmanager
pkgver=1.14.0
pkgrel=18
pkgdesc="ModemManager library"
url="http://www.freedesktop.org/wiki/Software/ModemManager"
arch="all"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends_dev="libmm-glib"
makedepends="$depends_dev gobject-introspection-dev gtk-doc vala
	libgudev-dev polkit-dev linux-headers"
checkdepends="glib-dev"
options="!check" # https://bugs.freedesktop.org/show_bug.cgi?id=101197
subpackages="
	$pkgname-lang
	$pkgname-doc
	libmm-glib:libmm
	$pkgname-dev
	$pkgname-bash-completion
	$pkgname-dbg
	"
source="https://www.freedesktop.org/software/ModemManager/ModemManager-$pkgver.tar.xz
	$pkgname.rules
	$pkgname.initd
	lte_fixes_spr29x.patch
	els62_support.patch
	els62_atcmds_wa.patch
	els62_swwan_workaround.patch
	els62_no-gcap_workaround.patch
	els62_ocpp_connection_detection.patch
	els62_fix_log_message_level.patch
	els62_exit_if_probe_fails.patch
	els62_1100_wa.patch
	startup_scripts/S35modemcheck
	startup_scripts/S35modemctl
	startup_scripts/S35modemmanager
	dms_1290/sync_nm_mm_disabled_state.sh"

builddir="$srcdir"/ModemManager-$pkgver

build() {
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-polkit=permissive \
		--enable-gtk-doc \
		--disable-static \
		--with-dbus-sys-dir=/etc/dbus-1/system.d \
		--enable-vala=yes \
		--disable-all-plugins \
		--enable-plugin-cinterion \
		--with-at-command-via-dbus
	make
}

libmm() {
	cd "$builddir"
	mkdir -p "$subpkgdir"
	make DESTDIR="$subpkgdir" -C libmm-glib install
	# move dev files to modemmnager-dev
	mv -f "$subpkgdir/usr/include/libmm-glib" "$pkgdir/usr/include/"
	mv -f "$subpkgdir/usr/share/gir-1.0" "$pkgdir/usr/share/"
	rmdir "$subpkgdir/usr/include" "$subpkgdir/usr/share"
}

package() {
	make DESTDIR="$pkgdir" install
	make DESTDIR="$pkgdir" -C libmm-glib uninstall
	rmdir "$pkgdir"/usr/lib/girepository-1.0 # in libmm-glib
	rm -rf "$pkgdir"/usr/share/dbus-1/system-services #systemd-service
	mkdir -p "$pkgdir/usr/share/polkit-1/rules.d"
	install -m644 -D "$srcdir/$pkgname.rules" \
		"$pkgdir/usr/share/polkit-1/rules.d/01-org.freedesktop.ModemManager.rules"

	mkdir -p "$pkgdir/etc/init.d/"
	install -m755 -D $srcdir/S35modemcheck \
		             $srcdir/S35modemctl \
		             $srcdir/S35modemmanager \
		"$pkgdir/etc/init.d/"
	install -m755 -D $srcdir/sync_nm_mm_disabled_state.sh "$pkgdir/usr/bin/"
	# post-install message
	mkdir -p "$pkgdir/usr/share/doc/$pkgname"
	cat > $pkgdir/usr/share/doc/$pkgname/README.alpine <<EOF
If your USB modem shows up as a Flash drive when you plug it in:

install 'usb-modeswitch' to automatically switch to USB modem mode whenever you plug it in.
To control your modem without the root password: add your user account to the 'plugdev' group.
EOF
}

check() {
	make check
}

dbg() {
	mkdir -p "$subpkgdir"/usr/share
	touch "$subpkgdir/usr/share/mm.debug"
}

sha512sums="cf8af23b6e87ccd44566a3b1391c994d8dfc97b22f599e10cc1a2a9c8563cd8e034f38e768961fbcffe1639866b2ef85e53f1cbc2151d4888ce27f52c02efca9  ModemManager-1.14.0.tar.xz
8d736f477649e42c05b34ac55391353c7f0c17138d039e049b16b07624d86fd9968ef1aa14672a63decf8b2c0ae984a34a0770322198b326775efba58f566685  modemmanager.rules
e2769401c52c3fff0b8057c13c6f7f2e5656c2963239ecbd647a01e66a344f6852cf2ba065358248315945bdf6ebbc4976903565b12ace2d15df6586dcc1de5b  modemmanager.initd
d9ca636770ca53a1c78082566d844c697ca365affdeeb62446e23f52b60b0f28b1f7697a7e5801fd6d44741866f7cf03c1d0e00b125ef075b3217b604b965ac2  lte_fixes_spr29x.patch
82631b292c064fb0bb9bf0292b2d3b5c3e7277b7084c826570529df64a16579994543f12fccc1391718508740a6c506652818fdd46b614953b12b6fcb822dc0f  els62_support.patch
c9a50ded9b6de4a20fa6d628c1d87cb26fca2d1b25c529ef656517858518ff68260262342b3e64f7c090964700c988726fd4fc03e4983b46d2383b27bf64e0ba  els62_atcmds_wa.patch
eba2df354e34cc85a0a4ebd948504055a447575d55aaae771d7666060a1745dd468fa85d1733a7ad95b07fc5e88e528ff712d52d379376faf3e2856c3639845a  els62_swwan_workaround.patch
b7e40cddb893ccaf3195a0b2afd848e2ba020ac291f34767065c46763bb02a49482328193dc3a5f372417fa53fb642a5c0bdc357a9c0213c373bf31a44aaa842  els62_no-gcap_workaround.patch
a96f62ac81476974a0ba649949bf3554c12b61185b4a1e1344458ea432cb2128f99c66dda658f1597a43b514d67fcb0c90be6ce01216580dd6281dba730be8b6  els62_ocpp_connection_detection.patch
3a569b975c3e41031aea4fb6af7047693496e0508df98f7cfdad42686fa97ec551633e06f5d80def87c8e82ec9e927ff45f32bfd0297a6a140136fed86ffe927  els62_fix_log_message_level.patch
9bf0bd882f367f58ce46472dfd3c21faf4d258ccb87ce87d9d6175bc48b23e8d8e3152cd57d2e701230ba70beae1755f26b1c8dc2e3d6318625d74b67b723c8a  els62_exit_if_probe_fails.patch
42becbad8fb00627ca3b5c1c5e5744bccbedb13e08bf84257136c278656513e6f5c18a0dc0127add5be790f469f017abca6540271e37fd07c0e3ebbde148737f  els62_1100_wa.patch
2848cf3c93d1330684c11365a67dd80396bcf26168b2d5a5ab5038832e8a603edb59da46bd83c13e57eafb88b1316cbc5152dcc115c9ca5abc836464d7d3c43c  S35modemcheck
86420cd9d9e0aafc3a78f989c67413ac78bf5e0a8961f967a57b87cfa128231d04393d17409350233730729efe201a78fbd0ed2d2ac4c0dbaaa56220b8a67fce  S35modemctl
205b8d7a74e8d1553e0e07ece04d637782af95699e0481b8dc9357a0c72b120bc68e78ca9a620b6cc6fb71cf87b1d27619e5b5da456aa7bace9a2db3cf622cfb  S35modemmanager
ed58d9765a149df78d5521e2140ec3ce99db4f7ef9e8bc06b2776f78a08fb59ffc32d74854ff3ab20ab7049c814dab7b75269ebd0c062125ad49dc2903cf2d04  sync_nm_mm_disabled_state.sh"
