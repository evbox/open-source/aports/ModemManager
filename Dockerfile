# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

ARG ALPINE_VERSION="3.12"
ARG TARGET_ARCH="arm32v7"

FROM registry.hub.docker.com/${TARGET_ARCH}/alpine:${ALPINE_VERSION}

ARG EVB_INTERNAL_APK_REPOSITORY

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN \
    apk add --no-cache \
        alpine-sdk \
        zlib-dev \
        zlib \
    && \
    rm -rf "/var/cache/apk/"* && \
    adduser root abuild && \
    adduser -D -G abuild buildbot && \
    chgrp abuild "/etc/apk/keys/" && \
    chmod g+w "/etc/apk/keys"

RUN \
    apk add --no-cache \
        ${EVB_INTERNAL_APK_REPOSITORY:+--allow-untrusted --repository=${EVB_INTERNAL_APK_REPOSITORY}/internal --repository=${EVB_INTERNAL_APK_REPOSITORY}/upstream} \
        libmbim \
        libqmi \
        libgudev-dev && \
    rm -rf "/var/cache/apk/"*

COPY "./dockerfiles/mbim_qmi_lib_devel.tar.gz" "/"

RUN \
    tar zxf /mbim_qmi_lib_devel.tar.gz -C / && \
    rm /mbim_qmi_lib_devel.tar.gz

COPY "./bin/package_builder-alpine.sh" "/usr/local/bin/package_builder-alpine.sh"
COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
COPY "./dockerfiles/docker-entrypoint.sh" "/init"

ENTRYPOINT [ "/init" ]
