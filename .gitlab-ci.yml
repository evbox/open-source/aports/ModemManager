#
# Copyright (C) 2020 EVBox Intelligence B.V.
#

workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_IID'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^release\/v\d+.\d+$/'
    - if: '$CI_PIPELINE_SOURCE == "web"'

include:
  - project: 'evbox/devops/ci/includes'
    ref: master
    file:
      - 'deploy/packages.yml'
      - 'deploy/release-branch.yml'

stages:
  - prepare
  - package
  - branch
  - deploy
  - cleanup


# Common parameters
# =============================================================================
default:
  tags:
    - docker
    - qemu

.artifact_paths: &artifact_paths
  paths:
    - "output/**/*.apk"

.git_login: &git_login
  - git config --global user.name "${BOT_USER_NAME}"
  - git config --global user.email "${BOT_USER_MAIL}"
  - git config --global credential.helper "cache --timeout=2147483647"
  - |
    _git_credentials="url=${CI_SERVER_URL}"
    _git_credentials="${_git_credentials}\nusername=${BOT_USER_NAME}"
    _git_credentials="${_git_credentials}\npassword=${CI_PERSONAL_TOKEN}\n\n"
    printf "${_git_credentials}" | git credential approve

.git_logout: &git_logout
  - git credential-cache exit


# Git authentication
# ===========================================================================
.git:
  image: "registry.hub.docker.com/gitscm/git:latest"
  before_script:
    - *git_login
  after_script:
    - *git_logout
  variables:
    GIT_DEPTH: "0"


# Build environment
# =============================================================================
.docker:
  image: "registry.hub.docker.com/library/docker:stable"

prepare_build_environment:
  extends: .docker
  stage: prepare
  variables:
    TARGET_ARCH: "arm32v7"
  tags:
    - qemu
  script:
    - |
      docker build \
                    --no-cache \
                    --build-arg "TARGET_ARCH=${TARGET_ARCH}" \
                    --build-arg \
                        "EVB_INTERNAL_APK_REPOSITORY=${CI_APK_REPOSITORY_URL}/${EVBOX_REPO_BRANCH:-release/latest}" \
                    --pull \
                    --rm \
                    --tag "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
                    "./"

# Build alpine package
# =============================================================================
.packaging:
  image: "${TARGET_ARCH}/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  stage: package
  artifacts:
    paths:
      - "output/"
    expire_in: "1 day"
  before_script:
    - ln -s "${CI_SIGNING_KEY}" "/run/${CI_SIGNING_KEY_NAME}"
    - ln -s "${CI_SIGNING_KEY_PUB}" "/run/${CI_SIGNING_KEY_NAME}.pub"
  script:
    - |
      package_builder-alpine.sh -k "/run/${CI_SIGNING_KEY_NAME}" \
                                -r "${CI_APK_REPOSITORY_URL}/${EVBOX_REPO_BRANCH:-release}/latest/internal, \
                                    ${CI_APK_REPOSITORY_URL}/${EVBOX_REPO_BRANCH:-release}/latest/upstream, \
                                    http://dl-cdn.alpinelinux.org/alpine/v3.12/main, \
                                    http://dl-cdn.alpinelinux.org/alpine/v3.12/community"

  after_script:
    - unlink "/run/${CI_SIGNING_KEY_NAME}"
    - unlink "/run/${CI_SIGNING_KEY_NAME}.pub"

package_arm32v7:
  extends: .packaging
  variables:
    TARGET_ARCH: "arm32v7"
  tags:
    - qemu

# Deploy packages
# =============================================================================
# Done by templates


# Release branch
# =============================================================================
# Done by templates


# Cleanup container
# =============================================================================
cleanup_build_environment:
  image: "registry.hub.docker.com/library/docker:stable"
  stage: cleanup
  when: always
  script:
    - |
      rmi_if_exists()
      {
        if docker inspect --type image "${1}" 1> /dev/null; then
          docker rmi "${1}"
        fi
      }
    - rmi_if_exists "arm32v7/${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
